#ifndef LISTELEMENTMODEL_H
#define LISTELEMENTMODEL_H

#include <QObject>

class ListElementModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString surname READ surname WRITE setSurname NOTIFY surnameChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString initials READ initials WRITE setInitials NOTIFY initialsChanged)
    Q_PROPERTY(QString fullName READ fullName NOTIFY fullNameChanged)
    Q_PROPERTY(bool checked READ checked WRITE setChecked NOTIFY checkedChanged)
    Q_PROPERTY(bool checkBoxVisible READ checkBoxVisible WRITE setCheckBoxVisible NOTIFY checkBoxVisibleChanged)

public:
    explicit ListElementModel(QObject *parent = 0);

    QString fullName() const;
    QString surname() const;
    void setSurname(const QString &surname);
    QString name() const;
    void setName(const QString &name);
    QString initials() const;
    void setInitials(const QString &initials);
    bool checked() const;
    void setChecked(const bool checked);
    bool checkBoxVisible() const;
    void setCheckBoxVisible(const bool checkBoxVisible);

signals:
    void surnameChanged();
    void nameChanged();
    void initialsChanged();
    void checkedChanged();
    void fullNameChanged();
    void checkBoxVisibleChanged();

private:
    QString m_surname;
    QString m_name;
    QString m_initials;
    bool    m_checked;
    bool    m_checkBoxVisible;
};

#endif // LISTELEMENTMODEL_H
