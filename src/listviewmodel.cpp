#include "listviewmodel.h"
#include "listelementmodel.h"
#include <QDebug>
#include <QRandomGenerator>

constexpr int MAX_CONTACTS = 100;

static inline QString getInitials(const QString &surname, const QString &name)
{
    QString ret = "";

    if (!surname.isEmpty())
    {
        ret += surname.toUpper().front();
    }
    if (!name.isEmpty())
    {
        ret += name.toUpper().front();
    }
    return ret;
}

ListViewModel::ListViewModel(QObject *parent) :
    QObject(parent)
{
    QList<QString> names{"Степан", "Денис", "Артём", "Тимофей", "Михаил", "Андрей", "Роберт", "Семён", "Владимир", "Александр"};
    QList<QString> surnames{"Крылов", "Николаев", "Пономарев", "Киселев", "Покровский", "Соколов", "Ушаков", "Назаров", "Волков", "Дружинин"};

    for(int i = 0; i < MAX_CONTACTS; ++i)
    {
        quint32 nameIndex = QRandomGenerator::global()->bounded(0, names.size() - 1);
        quint32 surnameIndex = QRandomGenerator::global()->bounded(0, surnames.size() - 1);

        add(surnames[surnameIndex], names[nameIndex]);
    }
}

QList<QObject*> ListViewModel::data()
{
    return m_data;
}

void ListViewModel::add(const QString &surname, const QString &name)
{
    auto *element = new ListElementModel(this);
    element->setSurname(surname);
    element->setName(name);
    element->setInitials(getInitials(surname, name));
    element->setChecked(false);
    element->setCheckBoxVisible(false);
    m_data.append(element);

    std::sort(m_data.begin(), m_data.end(), [](const auto &arg1, const auto &arg2)
    {
        return qobject_cast<ListElementModel*>(arg1)->initials() < qobject_cast<ListElementModel*>(arg2)->initials();
    });
    setAlphabetMap();
}

void ListViewModel::deleteCheckedItem()
{
    m_data.removeIf([](const auto &obj)
    {
        return qobject_cast<ListElementModel*>(obj)->checked();
    });

    setAlphabetMap();
    emit dataChanged();
}

void ListViewModel::setAlphabetMap()
{
    if(!m_alphabet.isEmpty())
    {
        m_alphabet.clear();
    }

    for(auto &iter: m_data)
    {
        QString str = qobject_cast<ListElementModel*>(iter)->initials().isEmpty() ?
                    QString::fromLatin1("\0") : qobject_cast<ListElementModel*>(iter)->initials();

        if(m_alphabet.isEmpty())
        {
            m_alphabet[m_data.indexOf(iter)] = str.front();
            continue;
        }

        if(str.front() != m_alphabet.last())
        {
            m_alphabet[m_data.indexOf(iter)] = str.front();
        }

    }
}

QString ListViewModel::getCharacter(qsizetype index)
{
    return {m_alphabet.value(index)};
}

bool ListViewModel::isContainsIndex(qsizetype index)
{
    return m_alphabet.contains(index);
}

void ListViewModel::setAllCheckBoxVisible(bool state)
{
    for(auto &iter: m_data)
    {
        qobject_cast<ListElementModel*>(iter)->setCheckBoxVisible(state);
    }
}

void ListViewModel::setAllChecked(bool state)
{
    for(auto &iter: m_data)
    {
        qobject_cast<ListElementModel*>(iter)->setChecked(state);
    }
}
