#include "listelementmodel.h"
#include <QDebug>

ListElementModel::ListElementModel(QObject *parent) :
    QObject(parent)
{
}

QString ListElementModel::fullName() const
{
    QString ret = m_surname;
    if (!m_surname.isEmpty())
    {
        ret += " ";
    }
    return ret += m_name;
}

QString ListElementModel::surname() const
{
    return m_surname;
}

void ListElementModel::setSurname(const QString &surname)
{
    if (m_surname == surname)
    {
        return;
    }

    m_surname = surname;

    emit surnameChanged();
}

QString ListElementModel::name() const
{
    return m_name;
}

void ListElementModel::setName(const QString &name)
{
    if (m_name == name)
    {
        return;
    }

    m_name = name;

    emit nameChanged();
}

QString ListElementModel::initials() const
{
    return m_initials;
}

void ListElementModel::setInitials(const QString &initials)
{
    if (m_initials == initials)
    {
        return;
    }

    m_initials = initials;

    emit initialsChanged();
}

bool ListElementModel::checked() const
{
    return m_checked;
}

void ListElementModel::setChecked(const bool checked)
{
    if (m_checked == checked)
    {
        return;
    }

    m_checked = checked;

    emit checkedChanged();
}

bool ListElementModel::checkBoxVisible() const
{
    return m_checkBoxVisible;
}

void ListElementModel::setCheckBoxVisible(const bool checkBoxVisible)
{
    if (m_checkBoxVisible == checkBoxVisible)
    {
        return;
    }

    m_checkBoxVisible = checkBoxVisible;

    emit checkBoxVisibleChanged();
}
