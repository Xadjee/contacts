#ifndef LISTVIEWMODEL_H
#define LISTVIEWMODEL_H

#include <QObject>
#include <QList>
#include <QMap>

class ListViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> data READ data NOTIFY dataChanged)

public:
    explicit ListViewModel(QObject *parent = nullptr);

    QList<QObject*> data();

    Q_INVOKABLE void add(const QString &surname, const QString &name);
    Q_INVOKABLE void deleteCheckedItem();
    Q_INVOKABLE QString getCharacter(qsizetype index);
    Q_INVOKABLE bool isContainsIndex(qsizetype index);
    Q_INVOKABLE void setAllCheckBoxVisible(bool state);
    Q_INVOKABLE void setAllChecked(bool state);

private:
    QList<QObject*> m_data;
    QMap<qsizetype, QChar> m_alphabet;

    void setAlphabetMap();

signals:
    void dataChanged();
};

#endif // LISTVIEWMODEL_H
