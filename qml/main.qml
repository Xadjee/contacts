import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls.Material 2.15

import BackCore 1.0

Window {
    width: 350
    height: 600
    visible: true
    title: qsTr("Contacts")

    Material.accent: "#2683e7"
    Material.primary: "#ffffff"
    Material.background: "#ffffff"
    Material.foreground: "#d7e9ff"

    BarTop{
        id: barTop
        z:2
        onDeletePressed:{
            listViewModel.deleteCheckedItem()
            contactsList.forceActiveFocus()
        }
    }

    ContactsList{
        id: contactsList

        anchors.top: barTop.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        myData: listViewModel.data

        ListViewModel{
            id: listViewModel
        }

        onCheckBoxEnabled:{
            barTop.setVisibleButton(true)
        }

        Keys.onReleased: (event)=>{
            if (event.key === Qt.Key_Back || event.key === Qt.Key_Escape){
                listViewModel.setAllCheckBoxVisible(false)
                listViewModel.setAllChecked(false)
                barTop.setVisibleButton(false)
            }
        }
    }

    Component.onCompleted:{
        contactsList.forceActiveFocus()
    }
}
