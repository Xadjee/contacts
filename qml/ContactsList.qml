import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import BackCore 1.0

Rectangle{
    property alias myData: list.model
    property int heightElements: 80

    signal checkBoxEnabled()

    ListView{
        id: list

        anchors.fill: parent
        anchors.leftMargin: 6
        anchors.rightMargin: 6

        reuseItems: false
        spacing: 6
        focus: true

        delegate: Rectangle{
            id: item

            width: list.width
            height: alphaPointer.visible ? heightElements : heightElements - alphaPointer.height
            clip: true

            Rectangle{
                id: alphaPointer
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 12
                anchors.right: parent.right
                height: heightElements / 2
                visible: listViewModel.isContainsIndex(index)
                Text{
                    id: alphaText
                    anchors.bottom: parent.bottom
                    text: listViewModel.getCharacter(index)
                }
            }

            Rectangle{
                id: listElement

                anchors.top: alphaPointer.visible ? alphaPointer.bottom : item.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                color: Material.background

                radius: 5

                MouseArea {
                    anchors.fill: parent
                    propagateComposedEvents: true

                    onPressAndHold:{
                        listViewModel.setAllCheckBoxVisible(true)
                        checkBoxEnabled()
                    }

                    onPressed:{
                        if (check.visible){
                            selector.checked = !selector.checked
                            modelData.checked = selector.checked
                        }
                    }
                }

                Rectangle{
                    id: initials

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 12

                    width: 30
                    height: width
                    radius: width / 2.5
                    color: "#f5f6f8"

                    Text{
                        id: initialsText
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: modelData.initials
                    }
                }
                Rectangle{
                    id: name
                    anchors.verticalCenter: initials.verticalCenter
                    anchors.left: initials.right
                    anchors.leftMargin: 12
                    anchors.right: check.left

                    Text{
                        id: nameText
                        anchors.verticalCenter: parent.verticalCenter
                        text: modelData.fullName
                    }
                }

                Rectangle{
                    id: check
                    anchors.verticalCenter: initials.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 12

                    width: 40
                    height: width
                    visible: modelData.checkBoxVisible
                    color: "transparent"

                    CheckBox{
                        id: selector
                        anchors.fill: parent
                        checked: modelData.checked

                        onCheckStateChanged:
                        {
                            modelData.checked = checked
                            checked ? listElement.color = Material.foreground :
                                      listElement.color = Material.background
                        }
                    }
                }
            }
        }
    }
}
