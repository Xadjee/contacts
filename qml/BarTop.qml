import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Rectangle {
    signal deletePressed()

    function setVisibleButton(_visible){
        trashButton.visible = _visible
    }

    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    height: 56

    color:  Material.primary

    Button{
        id: trashButton

        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 24

        visible: false
        icon.name: "trash_icon"
        icon.color: "transparent"
        icon.source: "qrc:/img/trash_icon.ico"

        background: Rectangle{
            radius: height / 2
            color: parent.down ? Material.foreground :
                   (parent.hovered ? Material.foreground : Material.background)
        }

        onPressed: {
            deletePressed()
        }
    }
}
